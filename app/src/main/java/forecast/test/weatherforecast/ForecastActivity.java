package forecast.test.weatherforecast;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;

import forecast.test.weatherforecast.core.Forecast;
import forecast.test.weatherforecast.manager.ForecastManager;
import forecast.test.weatherforecast.builder.ForecastBuilder;

/**
 * Created by mateamargo on 09/10/2015.
 */
public class ForecastActivity extends Activity {

    private final static String TAG = "FORECAST";

    private Forecast forecast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forecast);

        Intent intent = getIntent();
        String data = intent.getStringExtra("data");

        try {
            forecast = ForecastBuilder.build(data);
            Log.i(TAG, "Forecast built for " + forecast.getCity().getName() + ", " + forecast.getCity().getCountry());

            ForecastManager.getInstance().setForecast(forecast);
        } catch (JSONException e) {
            Log.e(TAG, "Cannot build Forecast", e);
        }
    }
}
