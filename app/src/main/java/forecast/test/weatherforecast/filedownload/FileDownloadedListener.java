package forecast.test.weatherforecast.filedownload;

/**
 * Created by mateamargo on 10/10/2015.
 */
public interface FileDownloadedListener {

    void onFileDownloadSuccess(Object data);

    void onFileDownloadFailure(Throwable error);
}
