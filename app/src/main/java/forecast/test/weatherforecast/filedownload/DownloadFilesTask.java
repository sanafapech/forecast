package forecast.test.weatherforecast.filedownload;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import forecast.test.weatherforecast.cache.DownloadCache;
import forecast.test.weatherforecast.cache.NoCache;

/**
 * Created by mateamargo on 09/10/2015.
 * Referencia: http://developer.android.com/reference/android/os/AsyncTask.html
 *
 */
public class DownloadFilesTask  extends AsyncTask<URL, Integer, Void> {

    private FileDownloadedListener listener;
    private DownloadCache cache;

    private final static String TAG = "DOWNLOAD_FILE_TASK";

    public DownloadFilesTask(FileDownloadedListener listener) {
        this(listener, new NoCache());
    }

    public DownloadFilesTask(FileDownloadedListener listener, DownloadCache cache) {
        this.listener = listener;
        this.cache = cache;

    }

    // DownloadCache

    @Override
    protected Void doInBackground(URL... urls) {
        int count = urls.length;
        long totalSize = 0;
        for (int i = 0; i < count; i++) {
            URL theUrl = urls[i];

            OutputStream data = cache.getFile(theUrl.toString());

            if (data == null) {
                Log.i(TAG, "Download file " + theUrl.toString());
                data = downloadFile(theUrl);
                cache.putFile(theUrl.toString(), data);
            } else {
                Log.i(TAG, "File loaded from cache: " + theUrl.toString());
            }

            listener.onFileDownloadSuccess(data);
        }
        return null;
    }

    private OutputStream downloadFile(URL url) {
        int count;
        OutputStream output = null;
        InputStream input = null;

        try {
            URLConnection conection = url.openConnection();
            conection.connect();
            // getting file length
            int lenghtOfFile = conection.getContentLength();

            // input stream to read file - with 8k buffer
            input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream to write file
            output = new ByteArrayOutputStream();

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

            return output;
        } catch (Exception e) {
            listener.onFileDownloadFailure(e);
        } finally {
            try {
                output.flush();
                output.close();
                input.close();
            } catch (Exception e) {
                Log.e(TAG, "Cannot close the streams", e);
            }

        }

        return null;
    }
}
