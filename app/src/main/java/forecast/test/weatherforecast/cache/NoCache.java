package forecast.test.weatherforecast.cache;

import java.io.OutputStream;
import java.net.URL;

/**
 * Created by mateamargo on 12/10/2015.
 */
public class NoCache implements DownloadCache {
    @Override
    public OutputStream getFile(String key) {
        return null;
    }

    @Override
    public void putFile(String key, OutputStream data) {

    }
}
