package forecast.test.weatherforecast;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import forecast.test.weatherforecast.cache.InMemoryCache;
import forecast.test.weatherforecast.core.Weather;
import forecast.test.weatherforecast.manager.ForecastManager;
import forecast.test.weatherforecast.util.BitmapUtil;
import forecast.test.weatherforecast.util.DateUtil;
import forecast.test.weatherforecast.filedownload.DownloadFilesTask;
import forecast.test.weatherforecast.filedownload.FileDownloadedListener;

/**
 * Created by mateamargo on 12/10/2015.
 */
public class WeatherDetailsActivity extends Activity implements FileDownloadedListener {

    private final static String TAG = "WEATHER_DETAILS";

    private ImageView weatherIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_weather_details);

        Weather weather = ForecastManager.getInstance().getWeatherById(getIntent().getExtras().getDouble("id"));

        TextView min = (TextView) findViewById(R.id.weather_details_min);
        min.setText("Min: " + String.valueOf(weather.getTemperature().getMin()) + "°");

        TextView max = (TextView) findViewById(R.id.weather_details_max);
        max.setText("Max: " + String.valueOf(weather.getTemperature().getMax()) + "°");

        TextView avg = (TextView) findViewById(R.id.weather_details_avg);
        avg.setText("Prom: " + String.format("%.2f", weather.getTemperature().getMin() + weather.getTemperature().getMax() / 2) + "°");

        TextView date = (TextView) findViewById(R.id.weather_details_date);
        date.setText(DateUtil.getDDMMYYFormat(weather.getDate()) + ", " + DateUtil.getDayName(weather.getDate()));

        TextView main = (TextView) findViewById(R.id.weather_details_condition);
        main.setText(weather.getCondition().getMain() + ": " + weather.getCondition().getDescription());

        weatherIcon = (ImageView) findViewById(R.id.weather_details_icon);

        URL icon = null;
        try {
            icon = new URL(ForecastManager.getInstance().getIconURL().replace("{0}", weather.getCondition().getIcon()));
            new DownloadFilesTask(this, InMemoryCache.getInstance()).execute(icon);
        } catch (MalformedURLException e) {
            Log.e(TAG, "Cannot create icon URL", e);
        }

    }

    @Override
    public void onFileDownloadSuccess(final Object data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                weatherIcon.setImageBitmap(BitmapUtil.createBitmap((OutputStream) data));
            }
        });
    }

    @Override
    public void onFileDownloadFailure(Throwable error) {
        Log.e(TAG, "Could not download the icon", error);
    }
}
