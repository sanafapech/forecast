package forecast.test.weatherforecast.manager;

import forecast.test.weatherforecast.core.Forecast;
import forecast.test.weatherforecast.core.Weather;

/**
 * Created by mateamargo on 12/10/2015.
 */
public class ForecastManager {

    private static ForecastManager instance;

    private Forecast forecast;

    private String iconURL;

    public static synchronized ForecastManager getInstance() {
        if (instance == null) {
            instance = new ForecastManager();
        }

        return instance;
    }

    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
    }

    public Forecast getForecast() {
        return forecast;
    }

    public String getIconURL() {
        return iconURL;
    }

    public void setIconURL(String iconURL) {
        this.iconURL = iconURL;
    }

    public Weather getWeatherById(double id) {
        for (Weather weather: forecast.getWeathers()) {
            if (weather.getId() == id) {
                return weather;
            }
        }

        throw new RuntimeException("Invalid Weather " + String.valueOf(id));
    }

}
