package forecast.test.weatherforecast.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import forecast.test.weatherforecast.R;
import forecast.test.weatherforecast.WeatherDetailsActivity;
import forecast.test.weatherforecast.cache.InMemoryCache;
import forecast.test.weatherforecast.core.Weather;
import forecast.test.weatherforecast.manager.ForecastManager;
import forecast.test.weatherforecast.util.BitmapUtil;
import forecast.test.weatherforecast.util.DateUtil;
import forecast.test.weatherforecast.filedownload.DownloadFilesTask;
import forecast.test.weatherforecast.filedownload.FileDownloadedListener;

/**
 * Created by mateamargo on 10/10/2015.
 */
public class WeatherList extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recycler_weather, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        RecyclerView rv = (RecyclerView) getView().findViewById(R.id.rv_weather);

        LinearLayoutManager lm = new LinearLayoutManager(getActivity().getBaseContext());
        rv.setLayoutManager(lm);

        WeathersAdapter weathersAdapter = new WeathersAdapter(ForecastManager.getInstance().getForecast().getWeathers(), getActivity());
        rv.setAdapter(weathersAdapter);
    }
}

class WeatherHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView weatherIcon;
    TextView weatherDate;
    TextView weatherMax;
    TextView weatherMin;

    private double id;
    private Activity activity;

    public WeatherHolder(View itemView) {
        super(itemView);

        itemView.setOnClickListener(this);

        weatherIcon = (ImageView) itemView.findViewById(R.id.weather_icon);
        weatherDate = (TextView) itemView.findViewById(R.id.weather_date);
        weatherMax = (TextView) itemView.findViewById(R.id.weather_max);
        weatherMin = (TextView) itemView.findViewById(R.id.weather_min);
    }

    public void setId(double id) {
        this.id = id;
    }

    public double getId() {
        return this.id;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(activity, WeatherDetailsActivity.class);
        intent.putExtra("id", this.id);

        activity.startActivity(intent);
    }

}

class WeathersAdapter extends RecyclerView.Adapter<WeatherHolder> {

    private final static String TAG = "WEATHER_ITEM";
    private List<Weather> weathers;
    private final Activity activity;

    public WeathersAdapter(List<Weather> weathers, Activity activity) {
        this.weathers = weathers;
        this.activity = activity;
    }

    @Override
    public WeatherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_weathers_list, parent, false);
        return new WeatherHolder(v);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(WeatherHolder holder, int position) {
        Weather weather = weathers.get(position);

        try {
            URL icon = new URL(ForecastManager.getInstance().getIconURL().replace("{0}", weather.getCondition().getIcon()));
            new DownloadFilesTask(new WeatherIconDownload(this.activity, holder.weatherIcon), InMemoryCache.getInstance()).execute(icon);
        } catch (MalformedURLException e) {
            Log.e(TAG, "Cannot download the weather icon", e);
        }

        holder.weatherDate.setText(DateUtil.getDDMMYYFormat(weather.getDate()) + ", " + DateUtil.getDayName(weather.getDate()));
        holder.weatherMax.setText("Max: " + String.valueOf(weather.getTemperature().getMax()) + "°");
        holder.weatherMin.setText("Min: " + String.valueOf(weather.getTemperature().getMin()) + "°");

        holder.setId(weather.getId());
        holder.setActivity(activity);
    }

    public void onIconReady(Bitmap iconBmp, ImageView iconView) {
        // This is because threading issues setting the views

        iconView.setImageBitmap(iconBmp);
    }

    @Override
    public int getItemCount() {
        return weathers.size();
    }


}

class WeatherIconDownload implements FileDownloadedListener {

    private final static String TAG = "WEATHER_ICON_DOWNLOAD";

    private ImageView weatherIcon;
    private Activity activity;

    public WeatherIconDownload(Activity activity, ImageView weatherIcon) {
        this.activity = activity;
        this.weatherIcon = weatherIcon;
    }

    @Override
    public void onFileDownloadSuccess(Object data) {
        final Bitmap icon = BitmapUtil.createBitmap((OutputStream) data);

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                weatherIcon.setImageBitmap(icon);
            }
        });
    }

    @Override
    public void onFileDownloadFailure(Throwable error) {
        Log.e(TAG, "The icon could not be downloaded", error);
    }
}