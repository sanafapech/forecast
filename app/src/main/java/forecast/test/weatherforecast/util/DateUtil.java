package forecast.test.weatherforecast.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by mateamargo on 12/10/2015.
 */
public class DateUtil {

    public static String getDDMMYYFormat(Date date) {
        SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");

        return fmt.format(date);
    }

    public static String getDayName(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int day = c.get(Calendar.DAY_OF_WEEK);

        switch (day) {
            case 1:
                return "Domingo";
            case 2:
                return "Lunes";
            case 3:
                return "Martes";
            case 4:
                return "Miércoles";
            case 5:
                return "Jueves";
            case 6:
                return "Viernes";
            case 7:
                return "Sábado";

            default:
                throw new RuntimeException("Invalid day");
        }
    }
}
