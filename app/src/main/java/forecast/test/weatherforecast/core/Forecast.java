package forecast.test.weatherforecast.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mateamargo on 10/10/2015.
 */
public class Forecast {

    public Forecast() {
        weathers = new ArrayList<>();
    }

    private City city;

    private List<Weather> weathers;

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<Weather> getWeathers() {
        return weathers;
    }

    public void setWeathers(List<Weather> weathers) {
        this.weathers = weathers;
    }

    public void addWeather(Weather weather) {
        weathers.add(weather);
    }
}
