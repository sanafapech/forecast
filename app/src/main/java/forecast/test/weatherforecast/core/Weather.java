package forecast.test.weatherforecast.core;

import java.util.Date;

/**
 * Created by mateamargo on 08/10/2015.
 */
public class Weather {

    private double id;

    private Date date;

    private double windDirection; // degrees

    private double windSpeed;

    private double clouds; // %

    private Temperature temperature;

    private double pressure; // hPa

    private double humidity; // %

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    private WeatherCondition condition;

    public WeatherCondition getCondition() {
        return condition;
    }

    public void setCondition(WeatherCondition condition) {
        this.condition = condition;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(double windDirection) {
        this.windDirection = windDirection;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public double getClouds() {
        return clouds;
    }

    public void setClouds(double clouds) {
        this.clouds = clouds;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

}
