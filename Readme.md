http://api.openweathermap.org/data/2.5/forecast/daily?q=buenos%20aires&mode=json&units=metric&APPID=e51d4c62c316867a5e34996189a48e90

{
	"city":{
		"id":3435910,
		"name":"Buenos Aires",
		"coord":{"lon":-58.377232,"lat":-34.613152},
		"country":"AR",
		"population":0},
	
	"cod":"200",
	"message":0.02,
	"cnt":7,
	"list":[
			{
			"dt":1444489200,
			"temp":{"day":10.47,"min":8.92,"max":10.65,"night":8.92,"eve":10.65,"morn":10.47},
			"pressure":1041.08,
			"humidity":73,
			"weather":[
				{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}
			],
			"speed":6.62,
			"deg":122,
			"clouds":64
		},
		
		
		
		{"dt":1444575600,"temp":{"day":11.84,"min":9.21,"max":13.16,"night":10.82,"eve":12.98,"morn":9.21},"pressure":1039.77,"humidity":89,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":6.76,"deg":166,"clouds":80,"rain":0.55},
		{"dt":1444662000,"temp":{"day":14.25,"min":9.18,"max":16.41,"night":9.88,"eve":16.41,"morn":9.18},"pressure":1032.79,"humidity":90,"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"speed":4.07,"deg":184,"clouds":0},
		{"dt":1444748400,"temp":{"day":21.04,"min":15.92,"max":21.04,"night":17.4,"eve":18.41,"morn":15.92},"pressure":1025.65,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":4.93,"deg":6,"clouds":61,"rain":0.57},
		{"dt":1444834800,"temp":{"day":24.04,"min":17.93,"max":24.04,"night":20.18,"eve":21.43,"morn":17.93},"pressure":1015.75,"humidity":0,"weather":[{"id":501,"main":"Rain","description":"moderate rain","icon":"10d"}],"speed":6.5,"deg":4,"clouds":22,"rain":5.35},
		{"dt":1444921200,"temp":{"day":17.24,"min":12.53,"max":17.92,"night":12.53,"eve":14.94,"morn":17.92},"pressure":1017.56,"humidity":0,"weather":[{"id":500,"main":"Rain","description":"light rain","icon":"10d"}],"speed":13.28,"deg":251,"clouds":20,"rain":2.15},
		{"dt":1445007600,"temp":{"day":16.89,"min":10.72,"max":16.89,"night":10.72,"eve":14.41,"morn":12.76},"pressure":1033.41,"humidity":0,"weather":[{"id":800,"main":"Clear","description":"sky is clear","icon":"01d"}],"speed":6.59,"deg":228,"clouds":4}
	]
}